const Chairo = require('chairo');
const Hapi = require('hapi');
const async = require('async');

const server = new Hapi.Server();

server.connection({
  port: 8080,
  routes: {
    json: {
      space: 4
    }
  }
});

server.register({register: Chairo}, function (err) {

  server.seneca.client({host: 'catalogsvc', pins: [{role: 'catalog', cmd: 'getById'}]});
  server.seneca.client({host: 'imagesvc',   pins: [{role: 'image',   cmd: 'getById'}]});
  server.seneca.client({host: 'pricingsvc', pins: [{role: 'pricing', cmd: 'getById'}]});
  server.seneca.client({host: 'ratingsvc',  pins: [{role: 'rating',  cmd: 'getById'}]});
  server.seneca.client({host: 'stocksvc',   pins: [{role: 'stock',   cmd: 'getById'}]});

  server.start(function(err) {
    if (err) {
      console.log(err);
      throw(err);
    }
    console.log('Books API ready!');
  });

});

server.route({
  method: 'GET',
  path: '/',
  handler: function (request, reply) {

    async.parallel({
      catalog: function(callback) {
        request.seneca.act({role: 'catalog', cmd: 'getById', id: 1234}, callback);
      },
      image: function(callback) {
        request.seneca.act({role: 'image', cmd: 'getById', id: 1234}, callback);
      },
      pricing: function(callback) {
        request.seneca.act({role: 'pricing', cmd: 'getById', id: 1234}, callback);
      },
      rating: function(callback) {
        request.seneca.act({role: 'rating', cmd: 'getById', id: 1234}, callback);
      },
      stock: function(callback) {
        request.seneca.act({role: 'stock', cmd: 'getById', id: 1234}, callback);
      }
    }, function(err, results) {
      if(!err){
        var book = results.catalog[0];
        book.cover = results.image[0].url;
        book.pricing = results.pricing[0];
        book.ratings = results.rating[0];
        book.stock = results.stock[0].count;

        return reply(err, book);
      }

      return reply(err);
    });
  }
});