# Microservices Example #

The source code from Paul's demo at the Q1 2016 Cox Automotive Architecture Summit

![Microservice Diagram](diagram.png)

### What's here? ###

This repository contains the source code for the Books API itself.  Each microservice is in its own repository:

* [Catalog Service](https://bitbucket.org/coxautomotive/catalog-service)
* [Image Service](https://bitbucket.org/coxautomotive/image-service)
* [Pricing Service](https://bitbucket.org/coxautomotive/pricing-service)
* [Rating Service](https://bitbucket.org/coxautomotive/rating-service)
* [Sales Tax Service](https://bitbucket.org/coxautomotive/salestax-service)
* [Shipping Service](https://bitbucket.org/coxautomotive/shipping-service)
* [Stock Service](https://bitbucket.org/coxautomotive/stock-service)

### How do I try it? ###

Building on what Brian covered at the Summit, each of the individual microservices has been dockerized.  This project will build the container for the API, and download all the microservice containers from [Docker Hub](https://hub.docker.com/u/pofallon/).

To get started, install the [Docker Toolbox](https://www.docker.com/products/docker-toolbox), clone this repository and run `docker-compose up`.  The Books API will be available on port 8080 of your Docker Host IP (for example, `curl http://192.168.99.100:8080/`).

### One more thing... ###

Rather than leaving the services completely hard-coded, two of them have been migrated to query live data stores.  The catalog service queries Elastic Search and the Rating Service queries Redis.  See the `docker-compose.yml` file (or the individual microservices) for details.  This helps demonstrate the data independence that can be achieved using microservices.  We'll migrate more of them over time.
